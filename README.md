# gregcoin

## What is GregCoin?

GregCoin is a highly advanced cryptocurrency built atop open source and blockchain technologies.

## Obtaining GregCoin

100 GregCoins are awarded to individuals upon making their first contribution to this project.

To earn 100 GregCoins:

1. Fork this project
1. Add `<gitlab_username> +100` as a new line at bottom of `blockchain.txt`
1. Create a merge request on `gitlab.com/greg/gregcoin`

When the MR is merged into the `master` branch, you will officially receive 100 GregCoin, as reflected in the GregCoin Blockchain (`blockchain.txt`).

## Sending GregCoin

GregCoin can be sent to any `GitLab.com` user.

To send GregCoin, add the following information at the bottom of `blockchain.txt`

```plaintext
<sender_gitlab_username>    -<amount of gregcoin>
<recipient_gitlab_username> +<amount of gregcoin>
```

To validate this transaction, open a merge request. 

Upon merging to master, this transaction will be validated and recorded in the official GregCoin blockchain, at which time GregCoins will be distributed accordingly.

## Super secure

Every transaction recorded in blockchain.txt is secured using military-grade _dev random_ cryptographic algorithms executed in a highly advanced GitLab Continuous Integration environment. See `.gitlab-ci.yml` for details.

## Totally decentralized

GregCoin uses advanced decentralized "Git" technologies to distribute the blockchain across multiple computers.

## Keep your blockchain up to date!

The `master` branch of the `https://gitlab.com/greg/gregcoin.git` is the only official source for the GregCoin blockchain.

Before making any GregCoin transactions, please be sure to `git pull origin master`.

Those with a blockchain that diverges from `master` are expected to `git rebase` and resolve merge conflicts.
